/* eslint-disable class-methods-use-this */
import axios, { AxiosRequestConfig } from 'axios';

// axios.defaults.withCredentials = true;
axios.defaults.baseURL = 'http://localhost:4000/api';

axios.interceptors.request.use(
  (config) => {
    if (typeof window !== 'undefined') {
      const accessToken = localStorage.getItem('accessToken');
      if (accessToken) config.headers.Authorization = `Bearer ${accessToken}`;
    }
    return config;
  },
  (err) => Promise.reject(err),
);

class UseAxios {
  async get(url: string, option: AxiosRequestConfig | undefined = undefined) {
    return axios
      .get(url, option)
      .then((res) => ({
        data: res.data,
        err: false,
      }))
      .catch((err) => ({
        data: null,
        err: err.response?.data || err,
      }));
  }

  async post(url: string, data: unknown, option: AxiosRequestConfig | undefined = undefined) {
    return axios
      .post(url, data, option)
      .then((res) => ({
        data: res.data,
        err: false,
      }))
      .catch((err) => ({
        data: null,
        err: err.response?.data || err,
      }));
  }

  async put(url: string, data: unknown, option: AxiosRequestConfig | undefined = undefined) {
    return axios
      .put(url, data, option)
      .then((res) => ({
        data: res.data,
        err: false,
      }))
      .catch((err) => ({
        data: null,
        err: err.response?.data || err,
      }));
  }

  async patch(url: string, data: unknown, option: AxiosRequestConfig | undefined = undefined) {
    return axios
      .patch(url, data, option)
      .then((res) => ({
        data: res.data,
        err: false,
      }))
      .catch((err) => ({
        data: null,
        err: err.response?.data || err,
      }));
  }

  async delete(url: string, option: AxiosRequestConfig | undefined = undefined) {
    return axios
      .delete(url, option)
      .then((res) => ({
        data: res.data,
        err: false,
      }))
      .catch((err) => ({
        data: null,
        err: err.response?.data || err,
      }));
  }
}

export default new UseAxios();
