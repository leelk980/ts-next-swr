import { GetServerSideProps, NextPage } from 'next';
import { Button } from 'antd';
import styles from 'styles/index.module.scss';
import Main from 'components/main';

interface Props {
  launch: {
    mission: string;
    site: string;
    timestamp: number;
    rocket: string;
  };
}

const IndexPage: NextPage<Props> = ({ launch }) => {
  const date = new Date(launch.timestamp);
  return (
    <main>
      <h1 className={styles.title}>Next SpaceX Launch: {launch.mission}</h1>
      <p className={styles.description}>
        {launch.rocket} will take off from {launch.site} on {date.toDateString()}
      </p>
      <p className={styles.content}>
        {launch.rocket} will take off from {launch.site} on {date.toDateString()}
      </p>
      <Button type='primary'>Primary</Button>
      <Main />
    </main>
  );
};

export const getServerSideProps: GetServerSideProps<Props> = async (_ctx) => {
  const response = await fetch('https://api.spacexdata.com/v3/launches/next');
  const nextLaunch = await response.json();

  return {
    props: {
      launch: {
        mission: nextLaunch.mission_name,
        site: nextLaunch.launch_site.site_name_long,
        timestamp: nextLaunch.launch_date_unix * 1000,
        rocket: nextLaunch.rocket.rocket_name,
      },
    },
  };
};

export default IndexPage;
