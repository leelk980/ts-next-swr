import useAxios from 'hooks/useAxios';
import { GetStaticPaths, GetStaticProps, NextPage } from 'next';
import { useState } from 'react';
import useSWR from 'swr';

type Post = {
  id: number;
  userId: number;
  title: string;
  body: string;
};

type Props = {
  initPost: Post;
};

const IndexPage: NextPage<Props> = ({ initPost }) => {
  const [post] = useState(initPost);

  const { data, error } = useSWR(`/posts/2`);

  if (error) return <div>Error...</div>;
  if (!data) return <div>Loading...</div>;

  return (
    <>
      <div>{post.title}</div>
      <div>{data.title}</div>
    </>
  );
};

export const getStaticPaths: GetStaticPaths = async () => {
  const paths = [
    { params: { id: '1' } },
    { params: { id: '2' } },
    { params: { id: '99' } },
    { params: { id: '100' } },
  ];

  return {
    paths,
    fallback: 'blocking',
  };
};

export const getStaticProps: GetStaticProps<Props> = async (ctx) => {
  const { id } = ctx.params as { id: string };
  const { data, err } = await useAxios.get(`/posts/${id}`);

  if (err)
    return {
      notFound: true,
    };

  return {
    props: {
      initPost: data,
    },
  };
};

export default IndexPage;
