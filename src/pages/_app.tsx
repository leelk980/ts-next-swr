import type { AppProps } from 'next/app';
import { SWRConfig } from 'swr';
import useAxios from 'hooks/useAxios';
import 'styles/globals.scss';

require('styles/antd.less');

const fetcher = (url: string) => useAxios.get(url).then((res) => res.data);

function MyApp({ Component, pageProps }: AppProps): JSX.Element {
  return (
    <SWRConfig value={{ fetcher }}>
      {/* eslint-disable-next-line */}
      <Component {...pageProps} />
    </SWRConfig>
  );
}

export default MyApp;
