// eslint-disable-next-line
const withAntdLess = require('next-plugin-antd-less');

module.exports = withAntdLess({
  // optional
  lessVarsFilePath: './src/styles/antd.less',
  // optional
  lessVarsFilePathAppendToEndOfContent: false,
  // optional https://github.com/webpack-contrib/css-loader#object
  cssLoaderOptions: {},

  webpack(config) {
    return config;
  },

  future: {
    webpack5: true,
  },
});
