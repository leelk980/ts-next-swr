module.exports = {
  presets: [['next/babel']],
  plugins: [['import', { libraryName: 'antd', style: true }]],
};

// https://www.npmjs.com/package/next-plugin-antd-less
